$(".btn-modal").fancybox({
    'padding'    : 0
});

// tabs

$('.project-item').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('.project');

    $(this).closest('.project-nav').find('.project-item').removeClass('active');
    $(this).addClass('active');

    box.find('.project-content-item').removeClass('active');
    box.find(tab).addClass('active');
    box.toggleClass('open');
});

$('.btn-back').click(function(e) {
    e.preventDefault();
    $('.project').toggleClass('open');
});

$(".project-item").hover(
    function(){

        $(this).closest(".project-nav").addClass("hover");

    },function(){

        $(this).closest(".project-nav").removeClass("hover");
    });



$(document).ready(function(){

    $('.btn-send').click(function() {

        $('body').find('form:not(this)').children('div').removeClass('red'); //удаление всех сообщение об ошибке(валидатора)
        var answer = checkForm($(this).closest('form').get(0)); //ответ от валидатора
        if(answer != false)
        {
            var $form = $(this).closest('form'),
                type    =     $('input[name="type"]', $form).val(),
                name    =     $('input[name="name"]', $form).val(),
                phone   =     $('input[name="phone"]', $form).val(),
                email   =     $('input[name="email"]', $form).val();
            $.ajax({
                type: "POST",
                url: "form-handler.php",
                data: {name: name, phone: phone, type:type, email: email}
            }).done(function(msg) {
                $('form').find('input[type=text], textarea').val('');
                console.log('удачно');
                document.location.href = "./done.html";
            });
        }
    });
});
